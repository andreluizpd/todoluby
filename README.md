# TODO Luby

Aplicação criada em react para a adição de tarefas e salva-las em localStorage.

## Especificações e ferramentas utilizadas

Especificações:

- A estilização com https://styled-components.com
- O projeto é uma SPA
- Um campo de texto para digitar a tarefa e um botão adicionar.
- Validação no campo, para que a tarefa não seja incluída na lista sem conteúdo. A tarefa deve ter no mínimo 5 caracteres.
- Botão para ordenar a lista
- Gravar as tarefas em uma localstorage.

Ferramentas Utilizadas:

- Formik para a criação de forms
- Yup para validação de campos
- Redux para controle de estado universal da aplicação
- Router-React-Dom para rotas SPA dentro da aplicação

## Como iniciar a aplicação

Realize o clone a aplicação do bitbucket para seu computador

```
git clone git@bitbucket.org:andreluizpd/todoluby.git # Se você utilizar SSH para autenticação
git clone https://andreluizpd@bitbucket.org/andreluizpd/todoluby.git # Se você utilizar HTTPS para autenticação
```

Baixando dependências e iniciando a aplicação

NPM:

```
cd todo-luby
npm install # Instalar as dependências da aplicação
npm start # Inicia a aplicação
```

Yarn:

```
cd todo-luby
yarn # Instalar as dependências da aplicação
yarn start # Inicia a aplicação
```
