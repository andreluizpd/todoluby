import React from 'react';
import styled from 'styled-components';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

import Navbar from './components/layout/Navbar';
import AddTodo from './components/todos/AddTodo';
import NotFound from './components/pages/NotFound';
import Home from './components/pages/Home';
import store from './store';

import './App.css';

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <AppContainer>
          <Navbar />
          <Container>
            <Switch>
              <Route exact path='/' component={Home} />
              <Route exact path='/add' component={AddTodo} />
              <Route component={NotFound} />
            </Switch>
          </Container>
        </AppContainer>
      </Router>
    </Provider>
  );
};

const Container = styled.div`
  max-width: 1100px;
  margin: auto;
  overflow: hidden;
  padding: 0 2rem;
`;

const AppContainer = styled.div`
  @import url('https://fonts.googleapis.com/css?family=Roboto');
  box-sizing: border-box;
  margin: 0;
  padding: 0;
  font-family: 'Roboto', sans-serif;
  font-size: 1rem;
  line-height: 1.6;
  background-color: #fff;
  color: #333;
`;

export default App;
