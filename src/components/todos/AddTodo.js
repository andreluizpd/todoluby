import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import PropTypes from 'prop-types';

import BackBtn from '../layout/BackBtn';
import Alert from '../layout/Alert';
import { addTask } from '../../actions/taskActions';

const TaskSchema = Yup.object().shape({
  task: Yup.string()
    .min(5, 'Your task must be at least 5 characters')
    .required('You need to add some text to the task'),
});

const TodoItem = ({ addTask }) => {
  return (
    <div>
      <BackBtn />
      <Formik
        initialValues={{ task: '' }}
        validationSchema={TaskSchema}
        onSubmit={({ task }, { resetForm }) => {
          addTask(task);
          resetForm();
        }}
      >
        {({ errors, touched }) => (
          <Form>
            {errors.task && touched.task ? (
              <Alert message={errors.task} />
            ) : null}
            <TextField placeholder='Add a task...' name='task' type='text' />
            <Button type='submit' value='Add Task' />
          </Form>
        )}
      </Formik>
    </div>
  );
};

TodoItem.propTypes = {
  addTask: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  task: state.task,
});

const Button = styled.input`
  display: inline-block;
  background: #333333;
  padding: 0.4rem 1.3rem;
  font-size: 1rem;
  border: none;
  cursor: pointer;
  margin-right: 0.5rem;
  margin-top: 1rem;
  transition: opacity 0.2s ease-in;
  outline: none;
  color: #fff;
`;

const TextField = styled(Field)`
  display: block;
  width: 100%;
  padding: 0.4rem;
  font-size: 1.2rem;
  border: 1px solid #ccc;
`;

export default connect(mapStateToProps, { addTask })(TodoItem);
