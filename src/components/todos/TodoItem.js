import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const TodoItem = ({ task }) => {
  return (
    <Card>
      <h3>{task.task}</h3>
    </Card>
  );
};

TodoItem.propTypes = {
  task: PropTypes.object.isRequired,
};

const Card = styled.div`
  padding: 1rem;
  border: #ccc 1px dotted;
  margin: 0.7rem 0;
`;

export default TodoItem;
