import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import TodoItem from './TodoItem';
import { getTasks } from '../../actions/taskActions';

const TodoList = ({ task: { tasks }, getTasks }) => {
  useEffect(() => {
    getTasks();
    //eslint-disable-next-line
  }, []);

  return tasks.map(task => <TodoItem task={task} key={task.id} />);
};

TodoList.propTypes = {
  task: PropTypes.object.isRequired,
  getTasks: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  task: state.task,
});

export default connect(mapStateToProps, { getTasks })(TodoList);
