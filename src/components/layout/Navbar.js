import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Navbar = ({ title }) => {
  return (
    <Nav>
      <h1>{title}</h1>
    </Nav>
  );
};

Navbar.defaultProps = {
  title: 'Todo Luby',
};

Navbar.propTypes = {
  title: PropTypes.string.isRequired,
};

const Nav = styled.nav`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0.7rem 2rem;
  z-index: 1;
  width: 100%;
  opacity: 0.9;
  margin-bottom: 1rem;
  background: #dc3545;
  color: #fff;
`;

export default Navbar;
