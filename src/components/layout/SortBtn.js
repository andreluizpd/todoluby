import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';

import { sortTasks } from '../../actions/taskActions';

const SortBtn = ({ sortTasks }) => {
  return <Button onClick={() => sortTasks()}>Sort Tasks</Button>;
};

SortBtn.propTypes = {
  sortTasks: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  task: state.task,
});

const Button = styled.button`
  display: inline-block;
  background: #f4f4f4;
  padding: 0.4rem 1.3rem;
  font-size: 1rem;
  border: none;
  cursor: pointer;
  margin-right: 0.5rem;
  transition: opacity 0.2s ease-in;
  outline: none;
  color: #333;
`;

export default connect(mapStateToProps, { sortTasks })(SortBtn);
