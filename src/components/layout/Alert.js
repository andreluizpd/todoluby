import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Alert = ({ message }) => {
  return <AlertMessage>{message}</AlertMessage>;
};

Alert.propTypes = {
  message: PropTypes.string.isRequired,
};

const AlertMessage = styled.div`
  padding: 0.7rem;
  margin: 1rem 0;
  opacity: 0.9;
  background: #dc3545;
  color: #fff;
`;

export default Alert;
