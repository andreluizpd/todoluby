import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const AddBtn = ({ title }) => {
  return <Button to='/add'>{title}</Button>;
};

AddBtn.defaultProps = {
  title: 'Add TODO Item',
};

AddBtn.propTypes = {
  title: PropTypes.string.isRequired,
};

const Button = styled(Link)`
  display: inline-block;
  background: #333333;
  padding: 0.4rem 1.3rem;
  font-size: 1rem;
  border: none;
  cursor: pointer;
  margin-right: 0.5rem;
  transition: opacity 0.2s ease-in;
  outline: none;
  color: #fff;
`;

export default AddBtn;
