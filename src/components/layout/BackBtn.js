import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const BackBtn = ({ title }) => {
  return <Button to='/'>{title}</Button>;
};

BackBtn.defaultProps = {
  title: 'Back',
};

BackBtn.propTypes = {
  title: PropTypes.string.isRequired,
};

const Button = styled(Link)`
  display: inline-block;
  background: #f4f4f4;
  padding: 0.4rem 1.3rem;
  font-size: 1rem;
  border: none;
  cursor: pointer;
  margin-right: 0.5rem;
  margin-bottom: 0.5rem;
  transition: opacity 0.2s ease-in;
  outline: none;
  color: #333;
`;

export default BackBtn;
