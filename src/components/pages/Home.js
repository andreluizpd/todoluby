import React, { Fragment } from 'react';

import AddBtn from '../layout/AddBtn';
import TodoList from '../todos/TodoList';
import SortBtn from '../layout/SortBtn';

const Home = () => {
  return (
    <Fragment>
      <AddBtn />
      <SortBtn />
      <TodoList />
    </Fragment>
  );
};

export default Home;
