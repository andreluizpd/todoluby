import React from 'react';
import styled from 'styled-components';

import BackBtn from '../layout/BackBtn';

const NotFound = () => {
  return (
    <div>
      <BackBtn />
      <h1>Not Found</h1>
      <Paragraph>The page you are looking fore does not exist</Paragraph>
    </div>
  );
};

const Paragraph = styled.p`
  font-size: 1.5rem;
  margin-bottom: 1rem;
`;

export default NotFound;
