import { ADD_TASK, GET_TASKS, SORT_TASKS } from './types';
import { v4 as generateId } from 'uuid';

export const getTasks = () => {
  let tasks = JSON.parse(localStorage.getItem('todoItems'));

  if (!tasks) {
    tasks = [];
  }

  return {
    type: GET_TASKS,
    payload: tasks,
  };
};

export const addTask = task => {
  let tasks = JSON.parse(localStorage.getItem('todoItems'));

  if (!tasks) {
    tasks = [];
  }

  const stateTasks = [...tasks, { id: generateId(), task }];

  localStorage.setItem('todoItems', JSON.stringify(stateTasks));

  return {
    type: ADD_TASK,
    payload: stateTasks,
  };
};

export const sortTasks = () => {
  let tasks = JSON.parse(localStorage.getItem('todoItems'));

  if (!tasks) {
    tasks = [];
  }

  tasks.sort((a, b) => {
    var textA = a.task.toUpperCase();
    var textB = b.task.toUpperCase();
    return textA < textB ? -1 : textA > textB ? 1 : 0;
  });

  return {
    type: SORT_TASKS,
    payload: tasks,
  };
};
