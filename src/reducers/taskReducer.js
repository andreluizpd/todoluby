import { ADD_TASK, GET_TASKS, SORT_TASKS } from '../actions/types';

const initialState = {
  tasks: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TASK:
      return {
        ...state,
        tasks: action.payload,
      };
    case GET_TASKS:
      return {
        ...state,
        tasks: action.payload,
      };
    case SORT_TASKS:
      return {
        ...state,
        tasks: action.payload,
      };
    default:
      return state;
  }
};
